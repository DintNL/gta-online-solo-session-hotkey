
;----------------------------------------------------------;
;              GTA Online Solo Session Hotkey              ;
;----------------------------------------------------------;
; https://gitlab.com/DintNL/gta-online-solo-session-hotkey ;
;----------------------------------------------------------;

#SingleInstance Force
SetWorkingDir %A_ScriptDir%
if not A_IsAdmin
	Run *RunAs "%A_ScriptFullPath%"

F12::
    Run %comspec% /c "netsh advfirewall firewall add rule name="GTA Online Solo Session Hotkey" dir=in action=block protocol=UDP localport=6672",, HIDE
    Run %comspec% /c "netsh advfirewall firewall add rule name="GTA Online Solo Session Hotkey" dir=out action=block protocol=UDP localport=6672",, HIDE
    Sleep, 10000
    Run %comspec% /c "netsh advfirewall firewall delete rule name="GTA Online Solo Session Hotkey" dir=in",, HIDE
    Run %comspec% /c "netsh advfirewall firewall delete rule name="GTA Online Solo Session Hotkey" dir=out",, HIDE
Return