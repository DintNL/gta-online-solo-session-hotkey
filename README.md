# GTA Online Solo Session Hotkey

Want to have a solo public session without freezing the game and without having to alt-tab out of the game? Use this [AutoHotkey](https://www.autohotkey.com/) script!

It works by adding a rule to Windows Firewall that blocks UDP port 6672 for 10 seconds when you press the hotkey. The hotkey is F12 by default, but you can easily change this if you'd like to. To do so, just open the script in a text editor like Notepad and change the hotkey to [any other key](https://www.autohotkey.com/docs/KeyList.htm).

## Requirements
* [AutoHotkey](https://www.autohotkey.com/)

## Download
* [Click to download the GTA Online Solo Session Hotkey script](https://gitlab.com/DintNL/gta-online-solo-session-hotkey/-/raw/main/gta-online-solo-session-hotkey.ahk?inline=false)
